let roomCode = null;
let user = {};
let users = {};
let currentQuestion = {};
let currentAnswer = {};
let likes = 0;
let dislikes = 0;
let token = crypto.randomUUID();

let globalRoomCode = null;

async function postJson(handle, value) {
    return await fetch(`https://localhost:8081/${handle}`, {
        method: "POST",
        headers: {
            "Content-Type": "application/json; charset=utf-8",
        },
        body: JSON.stringify(value)
    });
}

const renderPages = [];


// ==== LobbyPage ======
function initLobbyPage() {
    const inputValues = {};

    async function createRoom(name) {
        try {
            const response = await postJson('rooms/create', {
                con_token: token,
                master_name: name,
                questions_per_user: 1
            });

            const data = await response.json();

            if (data.error) {
                // TODO
                // setErrorText(dinputValuesata.error);
                console.error(data.error);
                return;
            }

            users = data.room.users;
            user = data.user;
            user.token = data.token;
            globalRoomCode = data.room.code;
            setGameState("room");
        } catch (error) {
            console.error("Error creating room:", error);
        }
    }

    function isValidUserName(userName) {
        return userName && userName.trim() !== '' && !userName.includes('$');
    }

    const userNameInput = document.getElementById("user-name");
    inputValues.name = userNameInput.value;

    userNameInput.addEventListener('input', (e) => {
        inputValues.name = e.target.value;
        createButton.disabled = !isValidUserName(inputValues.name);
    });

    const createButton = document.getElementById("create-button");
    createButton.disabled = !isValidUserName(inputValues.name);
    createButton.onclick = async () => {
        await createRoom(inputValues.name);
    };

    return () => {
    };
}

// =====================

// ==== RoomPage =====
function initRoomPage() {
    const roomCodeText = document.getElementById("room-code-text");
    const userCountText = document.getElementById("user-count-text");
    const startGameButton = document.getElementById("start-game-button");

    async function startGame() {
        try {
            const response = await postJson("rooms/start", {
                room_code: globalRoomCode,
                master_token: user.token
            });

            const data = await response.json();
            if (data && data.error) {
                // TODO
                // setErrorText(data.error);
                console.error(data.error);
                return;
            }

            setGameState("game");

        } catch (error) {
            console.error("Error starting room:", error);
        }
    }

    startGameButton.addEventListener('click', startGame);

    function render() {
        roomCodeText.innerText = `Код комнаты: ${globalRoomCode}`;
        userCountText.innerText = `Количество участников: ${Object.keys(users).length - 1}`;

        if (user && user.is_master) {
            startGameButton.innerText = "Начать игру";
            startGameButton.disabled = false;
        } else {
            startGameButton.innerText = "Ждем скрам-мастера...";
            startGameButton.disabled = true;
        }
    }

    return render;
}

// ===================

// ==== GamePage =====
function initGamePage() {
    let textArea = '';

    async function handleLike() {
        try {
            const response = await postJson('question/like', {
                room_code: roomCode,
                token: user.token,
            });

            const data = await response.json();
            if (data && data.error) {
                console.error(data.error);
                return;
            }

            likes = data.likes;
            dislikes = data.dislikes;
        } catch (error) {
            console.error("Error liking question:", error);
        }
    }

    async function handleDislike() {
        try {
            const response = await postJson('question/dislike', {
                room_code: roomCode,
                token: user.token,
            });

            const data = await response.json();
            if (data && data.error) {
                console.error(data.error);
                return;
            }

            likes = data.likes;
            dislikes = data.dislikes;
        } catch (error) {
            console.error("Error disliking question:", error);
        }
    }

    async function submitAnswer(answer) {
        try {

            const response = await postJson('question/answer', {
                room_code: roomCode,
                token: user.token,
                answer
            });

            const data = await response.json();
            if (data && data.error) {
                console.error(data.error);
            }
        } catch (error) {
            console.error("Error submitting answer:", error);
        }
    }

    async function nextQuestion() {
        try {
            const response = await postJson('question/next', {
                room_code: roomCode,
                token: user.token,
            });

            const data = await response.json();
            if (data && data.error) {
                console.error(data.error);
            }
        } catch (error) {
            console.error("Error getting next question:", error);
        }
    }

    const questionsDiv = document.getElementById('questions-div');
    const answererText = document.getElementById("answerer-text");
    const questionText = document.getElementById("question-text");
    const questionImage = document.getElementById("question-image");
    const answerDiv = document.getElementById('answer-div');
    const likesDiv = document.getElementById('likes-div');
    const likeButton = document.getElementById('like-button');
    const likeCountSpan = document.getElementById('like-count-span');
    const dislikeButton = document.getElementById('dislike-button');
    const dislikeCountSpan = document.getElementById('dislike-count-span');
    const nextQuestionButton = document.getElementById('next-question-button');

    likeButton.addEventListener('click', handleLike);
    dislikeButton.addEventListener('click', handleDislike);
    nextQuestionButton.addEventListener('click', nextQuestion);

    function render() {
        if (currentQuestion && currentQuestion.type === 1) {
            questionsDiv.style.display = 'block';
            answererText.innerText = `Отвечает: ${currentQuestion.answerer}`;
            questionText.innerText = currentQuestion.question;
            questionImage.src = currentQuestion.image_url;

        } else {
            questionsDiv.style.display = 'none';
        }

        if (currentQuestion.answerer === user.name) {
            answerDiv.innerHTML = `<textarea class="texta" id="question-text-area"
                placeholder="Ваш ответ здесь...">
                </textarea>
                <button class="button" id="submit-answer-button">
                Ответить
                </button>`;

            document.getElementById('question-text-area').addEventListener('change',
                (e) => {
                    textArea = e.target.value;
                });
            const submitAnswerButton = document.getElementById('submit-answer-button');
            submitAnswerButton.addEventListener('click',
                () => submitAnswer(textArea));
            submitAnswerButton.disabled = !textArea.trim();
        } else {
            answerDiv.innerHTML = `<textarea class="texta" disabled value=${currentAnswer}></textarea>`;
        }

        if (currentQuestion && currentAnswer != null) {
            likesDiv.style.display = 'block';
            likeButton.disabled = currentQuestion.answerer === user.name;
            dislikeButton.disabled = currentQuestion.answerer === user.name;
            likeCountSpan.innerText = likes;
            dislikeCountSpan.innerText = dislikes;
        }

        if (user && user.is_master) {
            nextQuestionButton.style.display = 'block';
        } else {
            nextQuestionButton.style.display = 'none';
        }
    }

    return render;
}

// ===================

// ==== JoinPage ====
function initJoinPage() {
    const inputValues = {};

    async function joinRoom(name, roomCode) {
        try {
            const response = await postJson('rooms/join', {
                con_token: token,
                room_code: roomCode,
                user_name: name,
            });

            const data = await response.json();
            if (data.error) {
                // TODO
                // setErrorText(data.error);
                console.error(data.error);
                return;
            }

            users = data.room.users;
            user = data.user;
            user.token = data.token;
            globalRoomCode = data.room.code;
            setGameState("room");
        } catch (error) {
            console.error("Error joining room:", error);
        }
    }

    function isFormValid() {
        return Object.values(inputValues).every(val => val.trim() !== '' && !val.includes('$'));
    }

    const joinButton = document.getElementById('join-button');
    const userNameJoinInput = document.getElementById('user-name-join-input');

    userNameJoinInput.addEventListener('input', (e) => {
        inputValues.name = e.target.value;
        joinButton.disabled = !isFormValid();
    });

    const roomCodeJoinInput = document.getElementById('room-code-join-input');
    roomCodeJoinInput.addEventListener('input', (e) => {
        inputValues.code = e.target.value;
        joinButton.disabled = !isFormValid();
    });

    joinButton.disabled = !isFormValid();
    joinButton.addEventListener('click', () => joinRoom(inputValues.name, inputValues.code));

    inputValues.name = userNameJoinInput.value;
    inputValues.code = roomCodeJoinInput.value;

    return () => {
    };
}

// ==================


renderPages.push(initLobbyPage());
renderPages.push(initRoomPage());
renderPages.push(initGamePage());
renderPages.push(initJoinPage());


const Events = {
    'PING': 0,
    'USER_JOINED': 1,
    'USER_LEFT': 2,
    'GAME_STARTED': 5,
    'GAME_ENDED': 6,
    'ANSWER_SUBMITTED': 8,
    'NEXT_QUESTION': 9,
    'LIKES_CHANGED': 11
};

const webSocketUrl = `wss://localhost:8081/ws`;
const socket = new WebSocket(`${webSocketUrl}?token=${token}`);

const Pages = {
    start: document.getElementById('startPage'),
    join: document.getElementById('joinPage'),
    lobby: document.getElementById('lobbyPage'),
    room: document.getElementById('roomPage'),
    game: document.getElementById('gamePage'),
    end: document.getElementById('endPage'),
};

let gameState = null;

function setGameState(state) {
    if (gameState === state) {
        return;
    }

    gameState = state;
    console.debug('Set gameState to', gameState);
    updateUI();
}

function updateUI() {
    for (const page in Pages) {
        if (gameState === page) {
            Pages[page].style.display = 'block';
        } else {
            Pages[page].style.display = 'none';
        }
    }

    for (const render of renderPages) {
        render();
    }
}

document.getElementById('createSession').addEventListener('click', () => {
    setGameState('lobby');
});

document.getElementById('joinSession').addEventListener('click', () => {
    setGameState('join');
});

socket.addEventListener('open', (event) => {
    console.debug("Соединение с сервером установлено");
});

socket.addEventListener('message', (event) => {
    const eventData = JSON.parse(event.data);
    console.debug("Получено событие:", eventData);

    switch (eventData.type) {
        case Events.PING:
            socket.send('pong');
            break;
        case Events.USER_JOINED:
            users[eventData.name] = {
                name: eventData.name,
                profile_pic: eventData.profile_pic,
                is_master: eventData.is_master
            };
            updateUI();
            break;
        case Events.USER_LEFT:
            delete users[eventData.name];
            updateUI();
            break;
        case Events.GAME_STARTED:
            likes = 0;
            dislikes = 0;
            setGameState('game');
            break;
        case Events.GAME_ENDED:
            setGameState('end');
            break;
        case Events.ANSWER_SUBMITTED:
            currentAnswer = eventData.answer;
            updateUI();
            break;
        case Events.NEXT_QUESTION:
            currentQuestion = {
                question: eventData.question,
                type: eventData.question_type,
                answers: eventData.answers,
                answerer: eventData.answerer,
                image_url: eventData.image_url
            };

            currentAnswer = null;
            likes = 0;
            dislikes = 0;
            updateUI();
            break;
        case Events.LIKES_CHANGED:
            likes = eventData.likes;
            dislikes = eventData.dislikes;
            updateUI();
            break
        default:
            console.warn("Неизвестный тип события:", eventData.type);
    }
});

socket.addEventListener('close', (event) => {
    console.debug("Соединение с сервером закрыто");
});

setGameState('start');
